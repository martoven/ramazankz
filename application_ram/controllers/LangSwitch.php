<?php
    class LangSwitch extends CI_Controller
    {
        public function __construct() {
            parent::__construct();
        }
        
        function switchLanguage($language = "") {
            $language = ($language != "") ? $language : "russian";
            $this->session->set_userdata('site_lang', $language);
            redirect(base_url());
        }
    }