<?php
    class News extends CI_Controller {
        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('news_model');
            
            // $this->load->helper('html');         
            //     $this->lang->load('news', 'russian');

            $segment = $this->uri->segment(1); 
            if($segment == 'ru' || $segment == 'en') { 
            switch ($segment) { 
            case 'ru': { 
            $this->session->set_userdata('lang', 'russian'); 
            $this->lang->load('news', 'russian'); 
            } 
            break; 

            case 'en': { 
            $this->session->set_userdata('lang', 'english'); 
            $this->lang->load('news', 'english'); 
            } 
            break; 

            default: 
            break; 
            } 
            } 
            else { 
            if(!$this->session->userdata('lang')) { 
            $this->lang->load('news', 'russian'); 
            } 
            else { 
            $this->lang->load('news', $this->session->userdata('lang')); 
            } 
            }
            
        }
        
        public function index()
        {
            $data['news'] = $this->news_model->get_news();
            $data['title'] = $this->lang->line('title');
            $data['lang'] = $this->lang->line('lang');
            $data['test'] = $this->lang->line('test');
            
            
            $this->load->view('templates/header', $data);
            $this->load->view('news/index', $data);
            $this->load->view('templates/footer', $data);            
        }

        public function changeLang($lang) { 
            $this->session->set_userdata('lang', $lang); 
            $segmentToDelete = null; 
            if (strpos($_SERVER['HTTP_REFERER'], 'ru/')) { 
            $segmentToDelete = 'ru/'; 
            } 

            else if(strpos($_SERVER['HTTP_REFERER'], 'en/')) { 
            $segmentToDelete = 'en/'; 
            } 
            if($lang == 'russian') { 
            redirect(str_replace($segmentToDelete, 'ru/', $_SERVER['HTTP_REFERER'])); 
            } 

            else if($lang == 'english') { 
            redirect(str_replace($segmentToDelete, 'en/', $_SERVER['HTTP_REFERER'])); 
            } 
        } 

        public function addUrl() { 
            if(!$this->session->userdata('lang')) { 
                $this->lang->load('news', 'russian'); 
                redirect('ru' . str_replace('index.php/','', $_SERVER['PHP_SELF'])); 
            } 
            else { 
                $this->lang->load('news', $this->session->userdata('lang')); 
                switch ($this->session->userdata('lang')) { 
                case 'russian': { 
                redirect('ru' . str_replace('index.php/','', $_SERVER['PHP_SELF'])); 
                } 
                break; 

                case 'english': { 
                redirect('en' . str_replace('index.php/','', $_SERVER['PHP_SELF'])); 
                } 
                break; 
                } 
            } 
        } 

        
        public function view($slug = NULL)
        {
            $data['news_item'] = $this->news_model->get_news($slug);
            $data['lang'] = $this->lang->line('lang');
            $data['test'] = $this->lang->line('test');
            
            if (empty($data['news_item']))
            {
                show_404();
            }
            
            $data['title'] = $data['news_item']['title'];
            
            $this->load->view('templates/header', $data);
            $this->load->view('news/view', $data);
            $this->load->view('templates/footer', $data);
        }
        
        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $data['title'] = 'Make An Ad';
            $data['lang'] = $this->lang->line('lang');
            $data['test'] = $this->lang->line('test');
            
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('text', 'Text', 'required');
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('text', 'Text', 'required');
            
            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('news/create');
                $this->load->view('templates/footer');
                
            }
            else
            {
                $this->news_model->set_news();

                $this->load->view('templates/header', $data);
                $this->load->view('news/success');
                $this->load->view('templates/footer', $data);
            }
        }
        
    }