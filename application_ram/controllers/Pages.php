<?php
    class Pages extends CI_Controller {

     public function __construct()
        {
            parent::__construct();
            $this->load->model('news_model');
            
            // $this->load->helper('html');         
            //     $this->lang->load('news', 'russian');

            $segment = $this->uri->segment(1); 
            if($segment == 'ru' || $segment == 'en') { 
            switch ($segment) { 
            case 'ru': { 
            $this->session->set_userdata('lang', 'russian'); 
            $this->lang->load('news', 'russian'); 
            } 
            break; 

            case 'en': { 
            $this->session->set_userdata('lang', 'english'); 
            $this->lang->load('news', 'english'); 
            } 
            break; 
            default: 
            break; 
            } 
            } 
            else { 
            if(!$this->session->userdata('lang')) { 
            $this->lang->load('news', 'russian'); 
            } 
            else { 
            $this->lang->load('news', $this->session->userdata('lang')); 
            } 
            }
            
        }
        
        public function view($page = 'home')
        {
            // if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
            // {
            //     // Whoops, we don't have a page for that!
            //     show_404();
            // }
            $this->load->helper('html');
            $this->load->helper('url');
            $data['title'] = ucfirst($page); // Capitalize the first letter
            $data['lang'] = $this->lang->line('lang');
            $data['test'] = lang('test');
            
            $this->load->view('templates/header', $data);
            $this->load->view('pages/'.$page, $data);
            $this->load->view('templates/footer', $data);
        }


    }


